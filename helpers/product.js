const products = [
    {id: 1, title: "T-shirt", views: 154, purchases: 12, public: false},
    {id: 2, title: "Pants", views: 292, purchases: 9, public: true},
    {id: 3,title: "Coat", views: 30, purchases: 1, public: true},
    {id: 4,title: "Boots", views: 45, purchases: 3, public: false},
    {id: 5,title: "Socks", views: 12, purchases: 0, public: true},
    {id: 6,title: "Earrings", views: 133, purchases: 8, public: true},
]

module.exports = {
    productsPublic: () => {
        return products.filter(product => {
            return product.public
        })
    },
    checkIfProductExistsAndReturn: (productId) => {
        productId = parseInt(productId)
        let product = null;
        products.forEach(prod => {
            if (prod.id === productId) {
                product = prod
            }
        })
        return product
    }
}