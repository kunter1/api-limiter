require("dotenv").config();
const express = require("express");
const router = require("./router");
const redisClient = require("./redis");

redisClient.on("ready", function () {
    console.log("connected to redis!");
});


// Initialize express
const app = express();

const server = async () => {
    await redisClient.connect()
    const PORT = process.env.PORT || 4000;
    app.use("/api", router);
    app.set("trust proxy", true);
    app.listen(4000, () => console.log(`App listening on port ${PORT}`));
};
server();



// Export app
module.exports = app;
