// Imports
const express = require("express");
const bodyParser = require("body-parser");

const router = express.Router();
const productRouter = require("./routes/product");

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json({limit: "10mb"}));
router.use(
    productRouter
);

// Export router
module.exports = router;
