const express = require("express");
const productController = require("../controllers/product");
const verifyToken = require("../middleware/token");
const limiterObject = require("../middleware/limiter");

const limiterPrivate = limiterObject.limiter({
    routeType: "PRIVATE"
})

// Limiter is also configurable
const limiterPublic = limiterObject.limiter({
    routeType: "PUBLIC",
    //timeSeconds: 5,
    //limit: 10
})



const router = express.Router();

//private
router.post('/product/:id', verifyToken, limiterPrivate, productController.getProduct)
router.post('/product/:id/statistics/views', verifyToken, limiterPrivate, productController.getProductViews)
router.post('/product/:id/statistics/purchases', verifyToken, limiterPrivate, productController.getProductPurchases)

//public
router.post('/products/public', limiterPublic, productController.getProductsPublic)
router.post('/products/public/details', limiterPublic, productController.getProductsPublicDetails)




// Export router
module.exports = router;
