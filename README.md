**API LIMITER**

Used stack:
- Node.js
- Express.js
- Redis


**Setup**: 

1. Redis
    - https://redis.io/docs/getting-started/installation/
2. App
    - npm install
    - npm run start


**Environment variables:**
- PORT=
- TOKEN_LIMIT=
- TOKEN_LIMIT_SEC=
- IP_LIMIT=
- IP_LIMIT_SEC=
