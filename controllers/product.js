const productHelper = require("../helpers/product")

const getProduct = async (req, res) => {
    try {
        const productId = req.params.id
        const product = productHelper.checkIfProductExistsAndReturn(productId)

        if (!product) return res.status(400).json({status: "Invalid product"});

        return res.status(200).json(product);
    } catch (e) {
        return res.status(400).json({status: "Error"});
    }
}

const getProductViews = async (req, res) => {
    try {
        const productId = req.params.id
        const product = productHelper.checkIfProductExistsAndReturn(productId)

        if (!product) return res.status(400).json({status: "Invalid product"});

        const body = {
            id: product.id,
            views: product.views
        }
        return res.status(200).json(body);
    } catch (e) {
        return res.status(400).json({status: "Error"});
    }
}

const getProductPurchases = async (req, res) => {
    try {
        const productId = req.params.id
        const product = productHelper.checkIfProductExistsAndReturn(productId)

        if (!product) return res.status(400).json({status: "Invalid product"});

        const body = {
            id: product.id,
            purchases: product.purchases
        }
        return res.status(200).json(body);
    } catch (e) {
        return res.status(400).json({status: "Error"});
    }
}

const getProductsPublic = async (req, res) => {
    try {

        const productsPublic = productHelper.productsPublic()

        if (!productsPublic || productsPublic.length < 1) return res.status(400).json({status: "No products found"});

        const productsMinimized = productsPublic.map(product => {
            return {
                id: product.id,
                title: product.title
            }
        })
        const body = {
            products: productsMinimized
        }
        return res.status(200).json(body);
    } catch (e) {
        return res.status(400).json({status: "Error"});
    }
}

const getProductsPublicDetails = async (req, res) => {
    try {

        const productsPublic = productHelper.productsPublic()

        if (!productsPublic || productsPublic.length < 1) return res.status(400).json({status: "No products found"});

        const body = {
            products: productsPublic
        }
        return res.status(200).json(body);
    } catch (e) {
        return res.status(400).json({status: "Error"});
    }
}


const productController = {
    getProduct,
    getProductViews,
    getProductPurchases,
    getProductsPublic,
    getProductsPublicDetails
}
module.exports = productController