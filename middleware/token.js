const allowedTokens = ["fd92a234-8d08-11ed-a1eb-0242ac120002",
    "0e516614-8d09-11ed-a1eb-0242ac120002",
    "12cadea0-8d09-11ed-a1eb-0242ac120002"]



module.exports = async (req, res, next) => {
    const token = req.headers.token;

    if (!token) return res.status(401).json({status: "Auth error"});
    if (!allowedTokens.includes(token)) return res.status(401).json({status: "Auth error"});

    next();
};
