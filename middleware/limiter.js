const redisClient = require("../redis")
const moment = require("moment-timezone");
require("dotenv").config();

const tokenOptions = {
    timeSeconds: parseInt(process.env.TOKEN_LIMIT_SEC),
    limit: parseInt(process.env.TOKEN_LIMIT)
}
const ipOptions = {
    timeSeconds: parseInt(process.env.IP_LIMIT_SEC),
    limit: parseInt(process.env.IP_LIMIT)
}


module.exports.limiter = function (options) {
    return async (req, res, next) => {

        let limitKey;

        const currentTime = moment().tz("Europe/Helsinki")

        if (options.routeType === "PRIVATE") {
            const token = req.headers.token
            limitKey = token + ":" + req.route.path
            options = {...tokenOptions, ...options}
        }
        if (options.routeType === "PUBLIC") {
            let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            ip = ip.toString().replace('::ffff:', '');
            limitKey = ip + ":" + req.route.path
            options = {...ipOptions, ...options}
        }

        if (!limitKey) return next()


        try {
            const keyExists = await redisClient.exists(limitKey)

            if (keyExists !== 1) {
                let requestBody = {
                    'callCount': 1,
                    'requestTime': currentTime
                }
                await redisClient.set(limitKey, JSON.stringify(requestBody))
                return next()
            }


            const keyResponse = await redisClient.get(limitKey)
            let requestData = JSON.parse(keyResponse)
            requestData.callCount = parseInt(requestData.callCount)

            const diffInMilliseconds = moment(currentTime).diff(moment(requestData.requestTime));
            const secondsBetweenRequests = diffInMilliseconds / 1000

            if (secondsBetweenRequests > options.timeSeconds) {
                let requestBody = {
                    'callCount': 1,
                    'requestTime': currentTime
                }
                await redisClient.set(limitKey, JSON.stringify(requestBody))
                return next()
            }

            if (secondsBetweenRequests < options.timeSeconds) {
                if (requestData.callCount > options.limit) {
                    const nextAvailableTime = moment(requestData.requestTime).add(options.timeSeconds, 'seconds')
                    const responseBody = {
                        msg: "Too many requests",
                        api_limit: options.limit,
                        retry_at: nextAvailableTime
                    }
                    return res.status(429).send(responseBody)
                }
                requestData.callCount++
                await redisClient.set(limitKey, JSON.stringify(requestData))
                return next()
            }

        } catch (e) {
            console.log(e)
            return res.status(500).send({status: "Error"})
        }
    }
}


